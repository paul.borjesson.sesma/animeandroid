package com.example.anime;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.MyViewHolder> {

    private Context mContext;
    private List<Anime> animes;
    private String emailV;
    private String userid;
    private String JSON_INSERTFAV_URL = "https://joanseculi.com/edt69/insertfavorite.php";
    private String JSON_DELETEFAV_URL = "https://www.joanseculi.com/edt69/deletefavorite.php";

    public AnimeAdapter(Context mContext, List<Anime> animes, String emailV, String userid) {
        this.mContext = mContext;
        this.animes = animes;
        this.emailV = emailV;
        this.userid = userid;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.my_anime, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load("https://joanseculi.com/" + animes.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.animeImage);
        holder.animeTitle.setText(animes.get(position).getName());
        holder.animeDescription.setText(animes.get(position).getDescription());
        holder.animeYear.setText(animes.get(position).getYear());
        holder.animeCategory.setText(animes.get(position).getType());

        if (mContext instanceof FavoritesActivity) {
            holder.favImage.setImageResource(R.drawable.ic_favorite_true);
        } else if (animes.get(position).getFavorite() != null && animes.get(position).getFavorite() != "null") {
            if (animes.get(position).getFavorite().equals(userid)) {
                holder.favImage.setImageResource(R.drawable.ic_favorite_true);
            } else {
                holder.favImage.setImageResource(R.drawable.ic_favorite_false);
            }
        } else {
            holder.favImage.setImageResource(R.drawable.ic_favorite_false);
        }

        holder.favImage.setOnClickListener(view -> {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
                constantState=mContext.getResources().getDrawable(R.drawable.ic_favorite_true, mContext.getTheme()).getConstantState();
            } else {
                constantState=mContext.getResources().getDrawable(R.drawable.ic_favorite_true).getConstantState();
            }

            if (holder.favImage.getDrawable().getConstantState()==constantState){
                deleteFavorite(animes.get(position).getName());
                holder.favImage.setImageResource(R.drawable.ic_favorite_false);
            } else {
                insertFavorite(animes.get(position).getName());
                holder.favImage.setImageResource(R.drawable.ic_favorite_true);
            }
        });

        holder.clickme.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra("Title", animes.get(position).getName());
            intent.putExtra("Description", animes.get(position).getDescription());
            intent.putExtra("Year", animes.get(position).getYear());
            intent.putExtra("Category", animes.get(position).getType());
            intent.putExtra("Picture", "https://joanseculi.com/" + animes.get(position).getImage());
            intent.putExtra("animeId", animes.get(position).getAnimeid());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout clickme;
        ImageView animeImage;
        TextView animeTitle;
        TextView animeDescription;
        TextView animeYear;
        TextView animeCategory;
        ImageView favImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            clickme = itemView.findViewById(R.id.clickme);
            animeImage = itemView.findViewById(R.id.animeImage);
            animeTitle = itemView.findViewById(R.id.animeTitle);
            animeDescription = itemView.findViewById(R.id.animeDescription);
            animeYear = itemView.findViewById(R.id.animeYear);
            animeCategory = itemView.findViewById(R.id.animeCategory);
            favImage = itemView.findViewById(R.id.favImage);
        }
    }

    public void insertFavorite(String nameV) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                JSON_INSERTFAV_URL,
                response -> {Toast.makeText(mContext, "Favorite added", Toast.LENGTH_SHORT).show();},
                error -> {Toast.makeText(mContext, "Favorite doesn't exist", Toast.LENGTH_SHORT).show();}
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailV );
                params.put("anime", nameV);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void deleteFavorite(String nameV) {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                JSON_DELETEFAV_URL,
                response -> {Toast.makeText(mContext, "Favorite deleted", Toast.LENGTH_SHORT).show();},
                error -> {Toast.makeText(mContext, "Favorite does not exist", Toast.LENGTH_SHORT).show();}
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailV );
                params.put("anime", nameV);
                return params;
            }
        };
        queue.add(stringRequest);
    }

}
