package com.example.anime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;

public class LoginActivity extends AppCompatActivity {

    private Button signinBtn;
    private Button signupBtn;
    private EditText email;
    private TextInputLayout password;

    private String JSON_LOGIN_URL = "https://www.joanseculi.com/edt69/loginuser.php?email=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Hooks
        signinBtn = findViewById(R.id.button);
        signupBtn = findViewById(R.id.button2);
        email = findViewById(R.id.field1);
        password = findViewById(R.id.field2);

        signinBtn.setOnClickListener(view -> {
            login();
        });

        signupBtn.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(intent);
        });

    }

    private void login() {
        JSON_LOGIN_URL += email.getText() + "&password=";
        JSON_LOGIN_URL += password.getEditText().getText();
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_LOGIN_URL,
                null,
                response -> {
                    try {
                        String id = response.getString("id");
                        Intent intent = new Intent(LoginActivity.this, ListActivity.class);
                        intent.putExtra("USER_ID", response.getString("id"));
                        intent.putExtra("USER_NAME", response.getString("name"));
                        intent.putExtra("USER_EMAIL", response.getString("email"));
                        intent.putExtra("USER_PASSWORD", response.getString("password"));
                        intent.putExtra("USER_PHONE", response.getString("phone"));
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Toast.makeText(this, "User not found", Toast.LENGTH_SHORT).show();
                    Log.d("tag", "onErrorResponse: " + error.getMessage());
                }
        );
        queue.add(jsonObjectRequest);
    }
}