package com.example.anime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoritesActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private AnimeAdapter animeAdapter;
    private List<Anime> animes = new ArrayList<>();

    private String JSON_FAVORITES_URL = "https://joanseculi.com/edt69/animesfavorites.php?email=";

    private String USER_ID;
    private String USER_NAME;
    private String USER_EMAIL;
    private String USER_PASSWORD;
    private String USER_PHONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);

        receiveData();

        loadFavorites();

    }

    private void receiveData() {
        Intent i = getIntent();
        USER_ID = i.getStringExtra("USER_ID");
        USER_NAME = i.getStringExtra("USER_NAME");
        USER_EMAIL = i.getStringExtra("USER_EMAIL");
        USER_PASSWORD = i.getStringExtra("USER_PASSWORD");
        USER_PHONE = i.getStringExtra("USER_PHONE");
    }

    private void loadFavorites() {
        JSON_FAVORITES_URL += USER_EMAIL;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_FAVORITES_URL,
                null,
                response -> {
                    try {
                        JSONArray jsonArrayAnimes = response.getJSONArray("animesfavorites");
                        for (int i = 0; i < jsonArrayAnimes.length(); i++) {
                            JSONObject animeObject = jsonArrayAnimes.getJSONObject(i);
                            Anime anime = new Anime();
                            anime.setName(animeObject.getString("name"));
                            anime.setDescription(animeObject.getString("description"));
                            anime.setType(animeObject.getString("type"));
                            anime.setYear(animeObject.getString("year"));
                            anime.setImage(animeObject.getString("image"));
                            boolean found = false;
                            for (Anime a : animes) {                //Em duplica entrades si no
                                if (a.name.equals(anime.name)) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                animes.add(anime);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    recyclerView = findViewById(R.id.recyclerView2);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    animeAdapter = new AnimeAdapter(FavoritesActivity.this, animes, USER_EMAIL, USER_ID);
                    recyclerView.setAdapter(animeAdapter);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                Intent intent = new Intent(FavoritesActivity.this, ListActivity.class);
                intent.putExtra("USER_ID", USER_ID);
                intent.putExtra("USER_NAME", USER_NAME);
                intent.putExtra("USER_EMAIL", USER_EMAIL);
                intent.putExtra("USER_PASSWORD", USER_PASSWORD);
                intent.putExtra("USER_PHONE", USER_PHONE);
                startActivity(intent);
                return true;
            case R.id.profile:  //CAL ENVIAR INFORMACIÓ DE L'USUARI
                Intent intent2 = new Intent(FavoritesActivity.this, UserActivity.class);
                intent2.putExtra("USER_ID", USER_ID);
                intent2.putExtra("USER_NAME", USER_NAME);
                intent2.putExtra("USER_EMAIL", USER_EMAIL);
                intent2.putExtra("USER_PASSWORD", USER_PASSWORD);
                intent2.putExtra("USER_PHONE", USER_PHONE);
                startActivity(intent2);
                return true;
            case R.id.favorite:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}