package com.example.anime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    ImageView detailImage;
    TextView detailCategory;
    TextView detailTitle;
    TextView detailDescription;
    TextView detailYear;

    private List<Video> videos = new ArrayList<>();
    RecyclerView detailRView;
    MyAdapterDetail myAdapterDetail;

    String idAnime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        detailImage = findViewById(R.id.detailImage);
        detailCategory = findViewById(R.id.detailCategory);
        detailTitle = findViewById(R.id.detailTitle);
        detailDescription = findViewById(R.id.detailDescription);
        detailYear = findViewById(R.id.detailYear);

        detailTitle.setText(getIntent().getStringExtra("Title"));
        detailDescription.setText(getIntent().getStringExtra("Description"));
        detailYear.setText(getIntent().getStringExtra("Year"));
        detailCategory.setText(getIntent().getStringExtra("Category"));
        Picasso.get().load(getIntent().getStringExtra("Picture"))
                .fit()
                .centerCrop()
                .into(detailImage);

        idAnime = getIntent().getStringExtra("animeId");

        llistarVideos();

    }

    private void llistarVideos() {
        System.out.println("https://joanseculi.com/edt69/animevideos.php?idanime=" + idAnime);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://joanseculi.com/edt69/animevideos.php?idanime=" + idAnime,
                null,
                response -> {
                    System.out.println("https://joanseculi.com/edt69/animevideos.php?idanime=" + idAnime);
                    try {
                        JSONArray jsonArrayVideos = response.getJSONArray("animevideos");
                        for (int i = 0; i < jsonArrayVideos.length(); i++) {
                            JSONObject jsonVideo = jsonArrayVideos.getJSONObject(i);
                            Video video = new Video();
                            video.episode = jsonVideo.getString("episode");
                            video.url = jsonVideo.getString("url");
                            videos.add(video);
                        }
                    }  catch (JSONException e) {
                        e.printStackTrace();
                    }
                    detailRView = findViewById(R.id.detailRView);
                    detailRView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    myAdapterDetail = new MyAdapterDetail(this, videos);
                    detailRView.setAdapter(myAdapterDetail);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        requestQueue.add(jsonObjectRequest);
    }

    class MyAdapterDetail extends RecyclerView.Adapter<MyAdapterDetail.MyViewHolder> {
        private Context context;
        private List<Video> videos;

        public MyAdapterDetail(Context context, List<Video> videos) {
            this.context = context;
            this.videos = videos;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.my_episode, parent, false );
            return new MyAdapterDetail.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.btnEpisode.setText("Episode " + videos.get(position).episode);
            holder.btnEpisode.setOnClickListener(view -> {
                Intent intent = new Intent(context, VideoActivity.class);
                intent.putExtra("url", videos.get(position).url);
                context.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return videos.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            Button btnEpisode;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                btnEpisode = itemView.findViewById(R.id.btnEpisode);
            }
        }
    }

}