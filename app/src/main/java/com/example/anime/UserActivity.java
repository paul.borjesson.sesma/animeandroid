package com.example.anime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnUpdate;
    private Button btnDelete;
    private EditText nameField;
    private EditText emailField;
    private EditText passwordField;
    private EditText phoneField;
    private String JSON_UPDATE_URL = "https://www.joanseculi.com/edt69/updateuser.php";
    private String JSON_DELETE_URL = "https://www.joanseculi.com/edt69/deleteuser.php";

    private String USER_ID;
    private String USER_NAME;
    private String USER_EMAIL;
    private String USER_PASSWORD;
    private String USER_PHONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        //Hooks
        btnUpdate = findViewById(R.id.btnUpdate);
        btnDelete = findViewById(R.id.btnDelete);
        nameField = findViewById(R.id.field1User);
        emailField = findViewById(R.id.field2User);
        passwordField = findViewById(R.id.field3User);
        phoneField = findViewById(R.id.field4User);

        Intent i = getIntent();

        receiveData();

        //Fill fields
        nameField.setText(USER_NAME);
        emailField.setText(USER_EMAIL);
        passwordField.setText(USER_PASSWORD);
        phoneField.setText(USER_PHONE);

        btnUpdate.setOnClickListener(view -> {
            updateUser();
        });

        btnDelete.setOnClickListener(view -> {
            deleteUser();
        });
    }

    private void receiveData() {
        Intent i = getIntent();
        USER_ID = i.getStringExtra("USER_ID");
        USER_NAME = i.getStringExtra("USER_NAME");
        USER_EMAIL = i.getStringExtra("USER_EMAIL");
        USER_PASSWORD = i.getStringExtra("USER_PASSWORD");
        USER_PHONE = i.getStringExtra("USER_PHONE");
    }

    private void updateUser() {
        String nameV = nameField.getText().toString();
        String emailV = emailField.getText().toString();
        String passwordV = passwordField.getText().toString();
        String phoneV = phoneField.getText().toString();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                JSON_UPDATE_URL,
                response -> {Toast.makeText(getApplicationContext(), "User updated successfully", Toast.LENGTH_SHORT).show();},
                error -> {Toast.makeText(getApplicationContext(), "There was an error updating the user", Toast.LENGTH_SHORT).show();}
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nameV);
                params.put("email", emailV);
                params.put("password", passwordV);
                params.put("phone", phoneV);

                //Això farà que s'actualitzi al canviar de finestra en temps real
                USER_NAME = nameV;
                USER_PASSWORD = passwordV;
                USER_PHONE = phoneV;

                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void deleteUser() {
        String emailV = emailField.getText().toString();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                JSON_DELETE_URL,
                response -> {Toast.makeText(getApplicationContext(), "User deleted successfully", Toast.LENGTH_SHORT).show();},
                error -> {Toast.makeText(getApplicationContext(), "There was an error deleting the user", Toast.LENGTH_SHORT).show();}
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailV);
                return params;
            }
        };
        queue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                Intent intent = new Intent(UserActivity.this, ListActivity.class);
                intent.putExtra("USER_ID", USER_ID);
                intent.putExtra("USER_NAME", USER_NAME);
                intent.putExtra("USER_EMAIL", USER_EMAIL);
                intent.putExtra("USER_PASSWORD", USER_PASSWORD);
                intent.putExtra("USER_PHONE", USER_PHONE);
                startActivity(intent);
                return true;
            case R.id.profile:
                Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.favorite:
                Intent intent2 = new Intent(UserActivity.this, FavoritesActivity.class);
                intent2.putExtra("USER_ID", USER_ID);
                intent2.putExtra("USER_NAME", USER_NAME);
                intent2.putExtra("USER_EMAIL", USER_EMAIL);
                intent2.putExtra("USER_PASSWORD", USER_PASSWORD);
                intent2.putExtra("USER_PHONE", USER_PHONE);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}