package com.example.anime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    private Button signupBtn;
    private Button loginBtn;
    private EditText nameField;
    private EditText emailField;
    private TextInputLayout passwordField;
    private EditText phoneField;
    private String JSON_SIGNUP_URL = "https://joanseculi.com/edt69/createuser2.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Hooks
        signupBtn = findViewById(R.id.button);
        loginBtn = findViewById(R.id.button2);
        nameField = findViewById(R.id.field1);
        emailField = findViewById(R.id.field2);
        passwordField = findViewById(R.id.field3);
        phoneField = findViewById(R.id.field4);

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signup();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    private void signup() {
        String nameV = nameField.getText().toString();
        String emailV = emailField.getText().toString();
        String passwordV = passwordField.getEditText().getText().toString();
        String phoneV = phoneField.getText().toString();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                JSON_SIGNUP_URL,
                response -> {
                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                    startActivity(intent);
                },
                error -> {Toast.makeText(getApplicationContext(), "Error in the credentials", Toast.LENGTH_SHORT).show();}
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nameV);
                params.put("email", emailV);
                params.put("password", passwordV);
                params.put("phone", phoneV);
                return params;
            }
        };
        queue.add(stringRequest);
    }
}